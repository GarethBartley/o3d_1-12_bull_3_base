#include "Drill.h"

Drill::Drill()
{
	drillSceneNode = nullptr;
	drill = nullptr;
	Vector3 meshBoundingBox(0.0f, 0.0f, 0.0f);
	colShape = nullptr;
	dynamicsWorld = nullptr;
}

Drill::~Drill()
{

}

void Drill::createMesh(SceneManager* scnMgr)
{
	drill = scnMgr->createEntity("drillDrill.mesh");
}

void Drill::attachToNode(SceneNode* parent)
{
	drillSceneNode = parent->createChildSceneNode();
	drillSceneNode->attachObject(drill);
	drillSceneNode->setScale(200.0f, 200.0f, 200.0f);
	boundingBoxFromOgre();
}

void Drill::setScale(float x, float y, float z)
{
	drillSceneNode->setScale(x, y, z);
}

void Drill::setRotation(Vector3 axis, Radian rads)
{
	Quaternion quat(rads, axis);
	drillSceneNode->setOrientation(quat);
}

void Drill::setPosition(float x, float y, float z)
{
	drillSceneNode->setPosition(x, y, z);
}

void Drill::boundingBoxFromOgre()
{
	drillSceneNode->_updateBounds();
	const AxisAlignedBox& b = drillSceneNode->_getWorldAABB();
	Vector3 temp(b.getSize());
	meshBoundingBox = temp;
}

void Drill::createRigidBody(float bodyMass)
{
	colShape = new btBoxShape(btVector3(meshBoundingBox.x / 2.0f, meshBoundingBox.y / 2.0f, meshBoundingBox.z / 2.0f));

	btTransform startTransform;
	startTransform.setIdentity();

	Quaternion quat2 = drillSceneNode->_getDerivedOrientation();
	startTransform.setRotation(btQuaternion(quat2.x, quat2.y, quat2.z, quat2.w));

	Vector3 pos = drillSceneNode->_getDerivedPosition();
	startTransform.setOrigin(btVector3(pos.x, pos.y, pos.z));

	btScalar mass(bodyMass);

	bool isDynamic = (mass != 0.0f);

	btVector3 localInertia(0, 0, 0);

	if (isDynamic)
	{
		colShape->calculateLocalInertia(mass, localInertia);
	}

	btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
	btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, colShape, localInertia);
	body = new btRigidBody(rbInfo);
}

void Drill::addToCollisionShapes(btAlignedObjectArray<btCollisionShape*> &collisionShapes)
{
	collisionShapes.push_back(colShape);
}

void Drill::addToDynamicsWorld(btDiscreteDynamicsWorld* dynamicsWorld)
{
	this->dynamicsWorld = dynamicsWorld;
	dynamicsWorld->addRigidBody(body);
}