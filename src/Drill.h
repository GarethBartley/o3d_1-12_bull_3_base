#pragma once

#include "Ogre.h"
#include "btBulletCollisionCommon.h"
#include "btBulletDynamicsCommon.h"

using namespace Ogre;

class Drill
{
private:
	SceneNode* drillSceneNode;
	Entity* drill;
	Vector3 meshBoundingBox;

	btCollisionShape* colShape;
	btRigidBody* body;
	btDiscreteDynamicsWorld* dynamicsWorld;

public:
	Drill();
	~Drill();

	void createMesh(SceneManager* scnMgr);
	void attachToNode(SceneNode* parent);
	void setScale(float x, float y, float z);
	void setRotation(Vector3 axis, Radian angle);
	void setPosition(float x, float y, float z);
	void boundingBoxFromOgre();
	void createRigidBody(float mass);
	void addToCollisionShapes(btAlignedObjectArray<btCollisionShape*> &collisionShapes);
	void addToDynamicsWorld(btDiscreteDynamicsWorld* dynamicsWorld);
};