#include "LooseParts.h"

LooseParts::LooseParts()
{

}

LooseParts::~LooseParts()
{

}

//Renders an engine from the tank by its self
void LooseParts::setupTankEngine(Vector3 enginePos, SceneManager* scnMgr, btAlignedObjectArray<btCollisionShape*> &collisionShapes, btDiscreteDynamicsWorld* dynamicsWorld)
{
	Entity* engine = scnMgr->createEntity("player_tankEngine_Right_Front.mesh");
	engine->setCastShadows(true);

	SceneNode* thisSceneNode = scnMgr->getRootSceneNode()->createChildSceneNode();
	thisSceneNode->attachObject(engine);

	// Axis
	Vector3 axis(1.0, 1.0, 0.0);
	axis.normalise();


	//angle
	Radian rads(Degree(0));

	//quat from axis angle
	Quaternion quat(rads, axis);

	// thisSceneNode->setOrientation(quat);
	thisSceneNode->setScale(200.0, 200.0, 200.0);

	//get bounding box here.
	thisSceneNode->_updateBounds();
	const AxisAlignedBox& b = thisSceneNode->_getWorldAABB();
	thisSceneNode->showBoundingBox(true);

	thisSceneNode->setOrientation(quat);

	thisSceneNode->setPosition(enginePos);

	Vector3 meshBoundingBox(b.getSize());

	//create a dynamic rigidbody

	btCollisionShape* colShape = new btBoxShape(btVector3(meshBoundingBox.x / 2.0f, meshBoundingBox.y / 2.0f, meshBoundingBox.z / 2.0f));
	collisionShapes.push_back(colShape);

	/// Create Dynamic Objects
	btTransform startTransform;
	startTransform.setIdentity();

	Vector3 pos = thisSceneNode->_getDerivedPosition();
	startTransform.setOrigin(btVector3(pos.x, pos.y, pos.z));

	Quaternion quat2 = thisSceneNode->_getDerivedOrientation();
	startTransform.setRotation(btQuaternion(quat2.x, quat2.y, quat2.z, quat2.w));

	btScalar mass(1.f);

	//rigidbody is dynamic if and only if mass is non zero, otherwise static
	bool isDynamic = (mass != 0.f);

	btVector3 localInertia(0, 0, 0);
	if (isDynamic)
	{
		colShape->calculateLocalInertia(mass, localInertia);
	}

	//using motionstate is recommended, it provides interpolation capabilities, and only synchronizes 'active' objects
	btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
	btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, colShape, localInertia);
	btRigidBody* body = new btRigidBody(rbInfo);

	dynamicsWorld->addRigidBody(body);
}
