#include "Hanger.h"

Hanger::Hanger()
{
	hangerSceneNode = nullptr;
	hanger = nullptr;
	Vector3 meshBoundingBox(0.0f, 0.0f, 0.0f);
	colShape = nullptr;
	dynamicsWorld = nullptr;
}

Hanger::~Hanger()
{

}

void Hanger::createMesh(SceneManager* scnMgr)
{
	hanger = scnMgr->createEntity("hangerHanger.mesh");
}

void Hanger::attachToNode(SceneNode* parent)
{
	hangerSceneNode = parent->createChildSceneNode();
	hangerSceneNode->attachObject(hanger);
	hangerSceneNode->setScale(200.0f, 200.0f, 200.0f);
	boundingBoxFromOgre();
}

void Hanger::setScale(float x, float y, float z)
{
	hangerSceneNode->setScale(x, y, z);
}

void Hanger::setRotation(Vector3 axis, Radian rads)
{
	Quaternion quat(rads, axis);
	hangerSceneNode->setOrientation(quat);
}

void Hanger::setPosition(float x, float y, float z)
{
	hangerSceneNode->setPosition(x, y, z);
}

void Hanger::boundingBoxFromOgre()
{
	hangerSceneNode->_updateBounds();
	const AxisAlignedBox& b = hangerSceneNode->_getWorldAABB();
	Vector3 temp(b.getSize());
	meshBoundingBox = temp;
}

void Hanger::createRigidBody(float bodyMass)
{
	colShape = new btBoxShape(btVector3(meshBoundingBox.x / 2.0f, meshBoundingBox.y / 2.0f, meshBoundingBox.z / 2.0f));

	btTransform startTransform;
	startTransform.setIdentity();

	Quaternion quat2 = hangerSceneNode->_getDerivedOrientation();
	startTransform.setRotation(btQuaternion(quat2.x, quat2.y, quat2.z, quat2.w));

	Vector3 pos = hangerSceneNode->_getDerivedPosition();
	startTransform.setOrigin(btVector3(pos.x, pos.y, pos.z));

	btScalar mass(bodyMass);

	bool isDynamic = (mass != 0.0f);

	btVector3 localInertia(0, 0, 0);

	if (isDynamic)
	{
		colShape->calculateLocalInertia(mass, localInertia);
	}

	btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
	btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, colShape, localInertia);
	body = new btRigidBody(rbInfo);
}

void Hanger::addToCollisionShapes(btAlignedObjectArray<btCollisionShape*> &collisionShapes)
{
	collisionShapes.push_back(colShape);
}

void Hanger::addToDynamicsWorld(btDiscreteDynamicsWorld* dynamicsWorld)
{
	this->dynamicsWorld = dynamicsWorld;
	dynamicsWorld->addRigidBody(body);
}