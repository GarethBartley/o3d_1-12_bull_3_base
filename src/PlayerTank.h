#pragma once

#include "Ogre.h"
#include "btBulletDynamicsCommon.h"
#include "btBulletCollisionCommon.h"

using namespace Ogre;

class PlayerTank
{
private:
	SceneNode* tankSceneNode;
	Entity* tank;
	Vector3 meshBoundingBox;

	btCollisionShape* colShape;
	btRigidBody* body;
	btDiscreteDynamicsWorld* dynamicsWorld;
	Camera* playerCam;

public:
	PlayerTank();
	~PlayerTank();

	void createMesh(SceneManager* scnMgr);
	void attachToNode(SceneNode* parent);
	void setScale(float x, float y, float z);
	void setRotation(Vector3 axis, Radian angle);
	void setPosition(float x, float y, float z);
	void boundingBoxFromOgre();
	void createRigidBody(float mass);
	void addToCollisionShapes(btAlignedObjectArray<btCollisionShape*> &collisionShapes);
	void addToDynamicsWorld(btDiscreteDynamicsWorld* dynamicsWorld);
	void setMass(float mass);
	void update();

	btRigidBody* getBody();
	Vector3 getPosition();
	SceneNode* getNode();

	void SetCamera(Camera* cam);
	void updateCameraPosition(const FrameEvent& evt);
	
};

