#include "PlayerTank.h"

PlayerTank::PlayerTank()
{
	tankSceneNode = nullptr;
	tank = nullptr;
	Vector3 meshBoudingBox(0.0f, 0.0f, 0.0f);
	colShape = nullptr;
	dynamicsWorld = nullptr;
}

PlayerTank::~PlayerTank()
{

}

void PlayerTank::createMesh(SceneManager* scnMgr)
{
	tank = scnMgr->createEntity("playertankTank_Base.mesh");
}

void PlayerTank::attachToNode(SceneNode* parent)
{
	tankSceneNode = parent->createChildSceneNode();
	tankSceneNode->attachObject(tank);
	tankSceneNode->setScale(200.0f, 200.0f, 200.0f);
	boundingBoxFromOgre();
}

void PlayerTank::setScale(float x, float y, float z)
{
	tankSceneNode->setScale(x, y, z);
}

void PlayerTank::setRotation(Vector3 axis, Radian rads)
{
	Quaternion quat(rads, axis);
	tankSceneNode->setOrientation(quat);
}

void PlayerTank::setPosition(float x, float y, float z)
{
	tankSceneNode->setPosition(x, y, z);
}

void PlayerTank::boundingBoxFromOgre()
{
	tankSceneNode->_updateBounds();
	const AxisAlignedBox& b = tankSceneNode->_getWorldAABB();
	Vector3 temp(b.getSize());
	meshBoundingBox = temp;
}

void PlayerTank::createRigidBody(float bodyMass)
{
	colShape = new btBoxShape(btVector3(meshBoundingBox.x / 2.0f, meshBoundingBox.y / 2.0f, meshBoundingBox.z / 2.0f));
	
	btTransform startTransform;
	startTransform.setIdentity();
	
	Quaternion quat2 = tankSceneNode->_getDerivedOrientation();
	startTransform.setRotation(btQuaternion(quat2.x, quat2.y, quat2.z, quat2.w));

	Vector3 pos = tankSceneNode->_getDerivedPosition();
	startTransform.setOrigin(btVector3(pos.x, pos.y, pos.z));

	btScalar mass(bodyMass);

	bool isDynamic = (mass != 0.0f);

	btVector3 localInertia(0, 0, 0);

	if (isDynamic)
	{
		colShape->calculateLocalInertia(mass, localInertia);
	}

	btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
	btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, colShape, localInertia);
	body = new btRigidBody(rbInfo);

	body->setUserPointer((void*)this);
}

void PlayerTank::addToCollisionShapes(btAlignedObjectArray<btCollisionShape*> &collisionShapes)
{
	collisionShapes.push_back(colShape);
}

void PlayerTank::addToDynamicsWorld(btDiscreteDynamicsWorld* dynamicsWorld)
{
	this->dynamicsWorld = dynamicsWorld;
	dynamicsWorld->addRigidBody(body);
}

void PlayerTank::update()
{
	btTransform trans;

	if (body && body->getMotionState())
	{
		body->getMotionState()->getWorldTransform(trans);
		btQuaternion orientation = trans.getRotation();

		tankSceneNode->setPosition(Ogre::Vector3(trans.getOrigin().getX(), trans.getOrigin().getY(), trans.getOrigin().getZ()));
		tankSceneNode->setOrientation(Ogre::Quaternion(orientation.getW(), orientation.getX(), orientation.getY(), orientation.getZ()));
	}
}

btRigidBody* PlayerTank::getBody()
{
	return body;
}

Vector3 PlayerTank::getPosition()
{	
	return tankSceneNode->getPosition();
}

void PlayerTank::SetCamera(Camera* cam)
{
	playerCam = cam;
	playerCam->setPosition(Vector3(0, 200, 300));
}

void PlayerTank::updateCameraPosition(const FrameEvent& evt)
{
	Vector3 camPos = playerCam->getPosition();
	Vector3 playerPos = tankSceneNode->getPosition();
	const float diffX = camPos.x - playerPos.x;
	const float diffY = camPos.y - playerPos.y;
	const float diffZ = camPos.z - playerPos.z;

	if (diffX * diffX + diffY * diffY + diffZ * diffZ > 400)
	{
		camPos.x -= (camPos.x - playerPos.x) * evt.timeSinceLastFrame;
		camPos.y -= (camPos.y - playerPos.y - 200) * evt.timeSinceLastFrame;
		camPos.z -= (camPos.z - playerPos.z - 450) * evt.timeSinceLastFrame;

		playerCam->setPosition(camPos);
	}
	playerCam->lookAt(tankSceneNode->getPosition());
}

SceneNode* PlayerTank::getNode()
{
	return tankSceneNode;
}