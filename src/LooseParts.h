#pragma once

#include "Ogre.h"
#include "btBulletCollisionCommon.h"
#include "btBulletDynamicsCommon.h"

using namespace Ogre;

//This class will be used to render parts of the models seprate from the rest.
class LooseParts
{
private:

public:
	LooseParts();
	~LooseParts();

	void setupTankEngine(Vector3 eningePos, SceneManager* scnMgr, btAlignedObjectArray<btCollisionShape*> &collisionShapes, btDiscreteDynamicsWorld* dynamicsWorld);
	
};

